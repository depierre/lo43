#include "Liste.cc"

int main(int argc, char *argv[])
{
    cout << " LO43 - TP2 - Exo 2" << endl << endl;

    // Test of Liste
    cout << "New empty Liste of int" << endl;
    Liste<int> liste1;
    int a = 12, b = 21, c = 24, d = 42;
    int e = -12, f = -21, g = -24, h = -42;

    cout << "Liste 1: add 4 elements (12, 21, 24 and 42)" << endl;
    liste1[0] = a;
    liste1.add(-1, b);
    liste1.add(-1, c);
    liste1.add(-1, d);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Length of Liste 1 is now: " << liste1.length() << endl;

    cout << "Liste 1: delete from the 2nd element" << endl;
    liste1.del_from(2);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Length of Liste 1 is now: " << liste1.length() << endl;

    Liste<int> liste2;

    cout << "Liste 2: add 4 elements (-12, -21, -24 and -42)" << endl;
    liste2[0] = e;
    liste2.add(-1, f);
    liste2.add(-1, g);
    liste2.add(-1, h);
    cout << "----" << endl;
    liste2.print_liste();
    cout << "----" << endl;

    cout << "Concatenation of the two lists (liste3 = liste1.concat(liste2))" << endl;
    Liste<int> liste3 = liste1.concat(liste2);

    cout << "----" << endl;
    liste3.print_liste();
    cout << "----" << endl;

    cout << "Length of Liste 3 is now: " << liste3.length() << endl;

    return 0;
}
