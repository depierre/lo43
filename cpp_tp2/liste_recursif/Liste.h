#ifndef __LISTE_H
#define __LISTE_H

// Create a generic list class (with template) with a recursive implementation
//
// Auteur : JC. Cr�put
// 20/03/2007
//

// Class Liste
template <class T> class Liste
{
    T val;
    Liste* suiv;

    public:
        // Coplien's canonical form
        Liste() ;
        Liste(const Liste& list);
        ~Liste();
        Liste& operator=(const Liste&list );

        // Add / delete
        void add(int index, T& element);
        void del_from(int index);
        // Value / modification
        T& operator[] (int index);
        // Observation (length)
        int is_empty();
        int length();
        // Concatenation (standard and self-concatenation)
        Liste concat(Liste &list);
        // Inputs-outputs
        void print_liste();
};

#endif /* __LISTE_H */
