#include "Liste.h"
#include <cstdlib>
#include <iostream>

using namespace std;

template<class T> Liste<T>::Liste()
{
    suiv = NULL;
    val = 0;
}

template<class T> Liste<T>::Liste(const Liste<T>& list)
{
    val = list.val;
    suiv = (list.suiv == NULL) ? NULL : new Liste(*list.suiv);
}

template<class T> Liste<T>::~Liste()
{
    if (suiv)
        delete suiv;
}

template<class T> Liste<T>& Liste<T>::operator=(const Liste<T>& list)
{
    val = list.val;
    delete suiv;
    suiv = new Liste(*list.suiv);

    return this;
}

template<class T> void Liste<T>::add(int index, T& element)
{
    if (index == 0 || suiv == NULL)
    {
        Liste* temp = new Liste();
        temp->suiv = suiv;
        temp->val = element;
        suiv = temp;
    }
    else
        suiv->add(index - 1, element);
}

template<class T> void Liste<T>::del_from(int index)
{
    if (index <= 1 || suiv == NULL)
    {
        this->suiv = NULL;
        delete suiv;
    }
    else
        suiv->del_from(index - 1);
}

template<class T> T& Liste<T>::operator[](int index)
{
    if (index == 0 || suiv == NULL)
        return val;
    else
        return (*suiv)[index - 1];
}

template<class T> int Liste<T>::is_empty()
{
    return (this == NULL) ? 1 : 0;
}

template<class T> int Liste<T>::length()
{
    int length = 0;
    Liste<T>* temp = this;

    while (temp)
    {
        temp = temp->suiv;
        length++;
    }
    return length;
}

template<class T> Liste<T> Liste<T>::concat(Liste<T>& list)
{
    Liste result(*this);
    Liste* temp = &list;

    while (temp != NULL)
    {
        result.add(-1, temp->val);
        temp = temp->suiv;
    }
    return result;
}

template<class T> void Liste<T>::print_liste()
{
    Liste<T>* temp = this;

    while (temp)
    {
        cout << temp->val << " ";
        temp = temp->suiv;
    }
    cout << endl;
}
