#include "Liste.h"

using namespace std;

template<class T> Liste<T>::Liste()
{
    _head = NULL;
    _tail = NULL;
}

template<class T> Liste<T>::Liste(Liste& list)
{
    if (list.is_empty())
    {
        _head = NULL;
        _tail = NULL;
    }
    else
        for (int i = 0; i < list.length(); i++)
            add_tail(list[i]);
}

template<class T> Liste<T>::~Liste()
{
    Maillon<T> *current;
    Maillon<T> *previous;

    if (_head != NULL)
    {
        current = _head;
        do
        {
            previous = current;
            current = current->_next;
            delete previous;
        } while (current != NULL);
        _head = NULL;
        _tail = NULL;
    }
}

template<class T> Liste<T>& Liste<T>::operator=(Liste& list)
{
    if (list.is_empty())
    {
        _head = NULL;
        _tail = NULL;
    }
    else
        for (int i = 0; i < list.length(); i++)
            add_tail(list[i + 1]);
    return *this;
}

template<class T> void Liste<T>::add_tail(T data)
{
    Maillon<T> *current;
    Maillon<T> *new_maillon;

    new_maillon = new Maillon<T>;
    new_maillon->_data = data;
    new_maillon->_next = NULL;

    if (_head == NULL)
    {
        _head = new_maillon;
        _tail = new_maillon;
    }
    else
    {
        current = _head;
        while (current->_next != NULL)
            current = current->_next;
        current->_next = new_maillon;
        _tail = new_maillon;
    }
}

template<class T> void Liste<T>::add_head(T data)
{
    Maillon<T> *current;
    Maillon<T> *new_maillon;

    new_maillon = new Maillon<T>;
    new_maillon->_data = data;

    if (_head == NULL)
    {
        new_maillon->_next = NULL;
        _head = new_maillon;
        _tail = new_maillon;
    }
    else
    {
        new_maillon->_next = _head;
        _head = new_maillon;
    }
}

template<class T> void Liste<T>::insert(int index, T data)
{
    Maillon<T> *current;
    Maillon<T> *new_maillon;

    new_maillon = new Maillon<T>;
    new_maillon->_data = data;

    if (index == 0)
        add_head(data);
    else if (index == length())
        add_tail(data);
    else if (index > 0 && index < length())
    {
        current = _head;
        for (int i = 1; i < index; i++)
            current = current->_next;
        if (current->_next == NULL)
        {
            new_maillon->_next = NULL;
            _tail = new_maillon;
            current->_next = new_maillon;
        }
        else
        {
            new_maillon->_next = current->_next;
            current->_next = new_maillon;
        }
    }
    else
        cout << "Invalid index" << endl;
}

template<class T> void Liste<T>::del_tail()
{
    Maillon<T> *current;

    if (_head != NULL)
    {
        current = _head;
        while (current->_next->_next != NULL)
            current = current->_next;
        delete current->_next;
        current->_next = NULL;
        _tail = current;
    }
}

template<class T> void Liste<T>::del_head()
{
    Maillon<T> *old_head;

    if (_head != NULL)
    {
        old_head = _head;
        _head = _head->_next;
        delete old_head;
    }
}

template<class T> void Liste<T>::del(int index)
{
    Maillon<T> *current;
    Maillon<T> *previous;

    if (index == 0)
        del_head();
    else if (index == length() - 1)
        del_tail();
    else if (index > 0 && index < length() - 1)
    {
        current = _head;
        for (int i = 1; i < index; i++)
            current = current->_next;
        previous = current->_next;
        current->_next = current->_next->_next;
        delete previous;
    }
    else
        cout << "Invalid index" << endl;
}

template<class T> T Liste<T>::operator[](int index)
{
    Maillon<T> *current;

    if (index == 0)
        return _head->_data;
    else if (index == length())
        return _tail->_data;
    else if (index > 0 && index <= length())
    {
        current = _head;
        for (int i = 1; i < index; i++)
            current = current->_next;
        return current->_data;
    }
    else
        cout << "Invalid index" << endl;
}

template<class T> void Liste<T>::print_liste() const
{
    Maillon<T> *current;
    int i = 0;

    if (_head != NULL)
    {
        current = _head;
        do
        {
            cout << "Liste[" << i << "] = " << current->_data << endl;
            current = current->_next;
            i++;
        } while (current != NULL);
    }
    else
        cout << "Liste is empty" << endl;
}

template<class T> int Liste<T>::is_empty()
{
    return (_head == NULL) ? 1 : 0;
}

template<class T> int Liste<T>::length()
{
    Maillon<T> *current;
    int i = 1;

    if (_head == NULL)
        return 0;
    current = _head;
    while (current->_next != NULL)
    {
        current = current->_next;
        i++;
    }
    return i;
}
