#ifndef __MAILLON_H
#define __MAILLON_H

template <class T> class Liste;

template <class T> class Maillon
{
    private:
        friend class Liste<T>;
        T _data;
        Maillon<T> *_next;
};

#endif //__MAILLON_H
