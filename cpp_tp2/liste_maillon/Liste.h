#ifndef __LISTE_H
#define __LISTE_H

#include <cstdlib>
#include <iostream>
#include "Maillon.h"

using namespace std;

// Class Liste
template <class T> class Liste {
    // Private fields
    private:
        Maillon<T> *_head;
        Maillon<T> *_tail;

    public:
        // Coplien's canonical form
        Liste();
        Liste(Liste& list);
        ~Liste();
        Liste& operator=(Liste& list);

        // Add elements
        void add_tail(T data);
        void add_head(T data);
        void insert(int index, T data);

        // Delete elements
        void del_tail();
        void del_head();
        void del(int index);

        // Access
        T operator[](int index);

        // Inputs-output
        // Constant function which cannot modify the class fields
        void print_liste() const;
        int is_empty();
        int length();
};

#endif //__LISTE_H
