#include <iostream>
#include <cstdlib>

// Application's include
#include "Liste.cc"

using namespace std;

int main(int argc, char **argv)
{
    cout << " LO43 - TP2 - Exo 1" << endl << endl;

    // Test of Liste
    cout << "New empty Liste of int" << endl;
    Liste<int> liste1;

    cout << "Add 2 elements at the head (12 then 21)" << endl;
    liste1.add_head(12);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;
    liste1.add_head(21);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;
    cout << "Add 2 elements at the tail (24 then 42)" << endl;
    liste1.add_tail(24);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;
    liste1.add_tail(42);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Length of Liste is now: " << liste1.length() << endl;

    cout << "Delete the current head" << endl;
    liste1.del_head();
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Delete the current tail" << endl;
    liste1.del_tail();
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Insert a new element at the index 1 (88)" << endl;
    liste1.insert(1, 88);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    cout << "Same again (but with 77 now)" << endl;
    liste1.insert(1, 77);
    cout << "----" << endl;
    liste1.print_liste();
    cout << "----" << endl;

    /* TODO: creation by reference bugs
    cout << "Creation of a new Liste by reference" << endl;
    Liste<int> liste2(liste1);
    cout << "New Liste contains:" << endl;
    cout << "----" << endl;
    liste2.print_liste();
    cout << "----" << endl;

    cout << "Delete all the Liste" << endl;
    liste2.~Liste();
    */
    liste1.~Liste();

    return 0;
}
